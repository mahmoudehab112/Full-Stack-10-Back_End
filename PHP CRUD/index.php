<!DOCTYPE html>
<html lang="en">

<head>
    <title>PHP CRUD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>
    <?php require_once 'process.php' ?>

    <?php

    if (isset($_SESSION['message'])):  ?>


    <div class="alert alert-<?=$_SESSION['msg-type']?>">
   
        <?php
            echo $_SESSION['message'];
            unset($_SESSION['message'])
        ?>
    </div>   
    <?php endif ?>
    <div class="container">
    <?php
        $mysqli = new mysqli('localhost' , 'root', '','crud','3308') or die(mysqli_error($mysqli));
        $result = $mysqli->query("SELECT * FROM data") or die($mysqli->error);
        // pre_r($result);
       ?>
        <div class="row justify-content-center">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Location</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
          <?php
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['location']; ?></td>
                    <td>
                        <a href="index.php?edit=<?php echo $row['id']; ?>"
                         class= "btn btn-info">Edit</a>
                        <a href="process.php?delete=<?php echo $row['id']; ?>"
                        class="btn btn-danger">Delete</a>
                        
                    </td>
                </tr>
            <?php endwhile; ?>        
            </table>
        </div> 
        
        <?php
        function pre_r ($array) {
            echo '<pre>';
            print_r($array);
            echo '</pre>';
        }
    ?>

    <form action="process.php" method="POST">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="text-center justify-content-center">
            <div class="row">
                <div class="col-md-4 offset-md-4 form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" 
                    value="<?php echo $name; ?>" placeholder="Enter your name">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 offset-md-4 form-group">
                    <label>Location</label>
                    <input type="text" name="location" class="form-control" 
                    value="<?php echo $name; ?>"placeholder="Enter your location">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 offset-md-4 form-group">
                    <?php 
                    if ($update == true):
                    ?>
                        <button type="submit" class="btn btn-info" name="update">Update</button>
                    <?php else: ?>
                    <button type="submit" class="btn btn-primary" name="save">Save</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>